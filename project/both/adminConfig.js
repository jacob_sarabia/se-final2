AdminConfig = {
  collections: {
    Users: {},
    PlayerRequest: {},
    PlayerResponse: {},
    Scenario : {},
    NPCharacter : {},
    NPCharacterRequest: {},
    NPCharacterResponse: {}
  }
}

/*,
  {
    "objectCode":"npC4",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["SLEEP_WHY"],
    "scenarioCodes":["SCENARIO_4"]

  },
  {
    "objectCode":"npC5",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT1-1"],
    "scenarioCodes":["SCENARIO_5"]

  },
  {
    "objectCode":"npC6",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT1-2"],
    "scenarioCodes":["SCENARIO_6"]

  },
  {
    "objectCode":"npC7",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_SLEEP"],
    "scenarioCodes":["SCENARIO_7"]

  },
  {
    "objectCode":"npC8",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT2-1_Dream"],
    "scenarioCodes":["SCENARIO_8"]

  },
  {
    "objectCode":"npC9",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT2-1_Transition"],
    "scenarioCodes":["SCENARIO_9"]

  },
  {
    "objectCode":"npC10",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT2-2_Dream"],
    "scenarioCodes":["SCENARIO_10"]

  },
  {
    "objectCode":"npC11",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT2-2_Transition"],
    "scenarioCodes":["SCENARIO_11"]

  },
  {
    "objectCode":"npC12",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT3-1_DreamEnding"],
    "scenarioCodes":["SCENARIO_12"]

  },
  {
    "objectCode":"npC13",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT3-2_RealEnding"],
    "scenarioCodes":["SCENARIO_13"]

  },
  {
    "objectCode":"npC14",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["ANX_ACT3-3_RealEnding"],
    "scenarioCodes":["SCENARIO_14"]

  },
  {
    "objectCode":"npC15",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT1-1"],
    "scenarioCodes":["SCENARIO_15"]

  },
  {
    "objectCode":"npC16",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT1-2"],
    "scenarioCodes":["SCENARIO_16"]

  },
  {
    "objectCode":"npC17",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_SLEEP"],
    "scenarioCodes":["SCENARIO_17"]

  },
  {
    "objectCode":"npC18",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT2-1_Dream"],
    "scenarioCodes":["SCENARIO_18"]

  },
  {
    "objectCode":"npC19",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT2-1_Transition"],
    "scenarioCodes":["SCENARIO_19"]

  },
  {
    "objectCode":"npC20",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT2-2_Dream"],
    "scenarioCodes":["SCENARIO_20"]

  },
  {
    "objectCode":"npC21",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT2-2_Transition"],
    "scenarioCodes":["SCENARIO_21"]

  },
  {
    "objectCode":"npC22",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT3-1_DreamEnding"],
    "scenarioCodes":["SCENARIO_22"]

  },
  {
    "objectCode":"npC23",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT3-2_RealEnding"],
    "scenarioCodes":["SCENARIO_23"]

  },
  {
    "objectCode":"npC24",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["STS_ACT3-3_RealEnding"],
    "scenarioCodes":["SCENARIO_24"]

  },
  {
    "objectCode":"npC25",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT1-1"],
    "scenarioCodes":["SCENARIO_25"]

  },
  {
    "objectCode":"npC26",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT1-2"],
    "scenarioCodes":["SCENARIO_26"]

  },
  {
    "objectCode":"npC27",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_SLEEP"],
    "scenarioCodes":["SCENARIO_27"]

  },
  {
    "objectCode":"npC28",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT2-1_Dream"],
    "scenarioCodes":["SCENARIO_28"]

  },
  {
    "objectCode":"npC29",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT2-1_Transition"],
    "scenarioCodes":["SCENARIO_29"]

  },
  {
    "objectCode":"npC30",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT2-2_Dream"],
    "scenarioCodes":["SCENARIO_30"]

  },
  {
    "objectCode":"npC31",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT2-2_Transition"],
    "scenarioCodes":["SCENARIO_31"]

  },
  {
    "objectCode":"npC32",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT3-1_DreamEnding"],
    "scenarioCodes":["SCENARIO_32"]

  },
  {
    "objectCode":"npC33",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT3-2_RealEnding"],
    "scenarioCodes":["SCENARIO_33"]

  },
  {
    "objectCode":"npC34",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["TRM_ACT3-3_RealEnding"],
    "scenarioCodes":["SCENARIO_34"]

  },
  {
    "objectCode":"npC35",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["FILL"],
    "scenarioCodes":["SCENARIO_35"]

  }*/