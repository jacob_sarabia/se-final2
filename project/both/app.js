/*****************************************************************************/
/* App: The Global Application Namespace */
/*****************************************************************************/
App = {};

/*
  {
    "objectCode":"SCENARIO_4",
    "difficulty": 1,
    "title":"4",
    "scenarioDescription":"4"
  },
  {
    "objectCode":"SCENARIO_5",
    "difficulty": 1,
    "title":"5",
    "scenarioDescription":"5"
  },
  {
    "objectCode":"SCENARIO_6",
    "difficulty": 1,
    "title":"6",
    "scenarioDescription":"6"
  },
  {
    "objectCode":"SCENARIO_7",
    "difficulty": 1,
    "title":"7",
    "scenarioDescription":"7"
  },
  {
    "objectCode":"SCENARIO_8",
    "difficulty": 1,
    "title":"8",
    "scenarioDescription":"8"
  },
  {
    "objectCode":"SCENARIO_9",
    "difficulty": 1,
    "title":"9",
    "scenarioDescription":"9"
  },
  {
    "objectCode":"SCENARIO_10",
    "difficulty": 1,
    "title":"10",
    "scenarioDescription":"10"
  },
  {
    "objectCode":"SCENARIO_11",
    "difficulty": 1,
    "title":"11",
    "scenarioDescription":"11"
  },
  {
    "objectCode":"SCENARIO_12",
    "difficulty": 1,
    "title":"12",
    "scenarioDescription":"12"
  },
  {
    "objectCode":"SCENARIO_13",
    "difficulty": 1,
    "title":"13",
    "scenarioDescription":"13"
  },
  {
    "objectCode":"SCENARIO_14",
    "difficulty": 1,
    "title":"14",
    "scenarioDescription":"14"
  },
  {
    "objectCode":"SCENARIO_15",
    "difficulty": 1,
    "title":"15",
    "scenarioDescription":"15"
  },
  {
    "objectCode":"SCENARIO_16",
    "difficulty": 1,
    "title":"16",
    "scenarioDescription":"16"
  },
  {
    "objectCode":"SCENARIO_17",
    "difficulty": 1,
    "title":"17",
    "scenarioDescription":"17"
  },
  {
    "objectCode":"SCENARIO_18",
    "difficulty": 1,
    "title":"18",
    "scenarioDescription":"18"
  },
  {
    "objectCode":"SCENARIO_19",
    "difficulty": 1,
    "title":"19",
    "scenarioDescription":"19"
  },
  {
    "objectCode":"SCENARIO_20",
    "difficulty": 1,
    "title":"20",
    "scenarioDescription":"20"
  },
  {
    "objectCode":"SCENARIO_21",
    "difficulty": 1,
    "title":"21",
    "scenarioDescription":"21"
  },
  {
    "objectCode":"SCENARIO_22",
    "difficulty": 1,
    "title":"22",
    "scenarioDescription":"22"
  },
  {
    "objectCode":"SCENARIO_23",
    "difficulty": 1,
    "title":"23",
    "scenarioDescription":"23"
  },
  {
    "objectCode":"SCENARIO_24",
    "difficulty": 1,
    "title":"24",
    "scenarioDescription":"24"
  },
  {
    "objectCode":"SCENARIO_25",
    "difficulty": 1,
    "title":"25,
    "scenarioDescription":"25"
  },
  {
    "objectCode":"SCENARIO_26",
    "difficulty": 1,
    "title":"26",
    "scenarioDescription":"26"
  },
  {
    "objectCode":"SCENARIO_27",
    "difficulty": 1,
    "title":"27",
    "scenarioDescription":"27"
  },
  {
    "objectCode":"SCENARIO_28",
    "difficulty": 1,
    "title":"28",
    "scenarioDescription":"28"
  },
  {
    "objectCode":"SCENARIO_29",
    "difficulty": 1,
    "title":"29",
    "scenarioDescription":"29"
  },
  {
    "objectCode":"SCENARIO_30",
    "difficulty": 1,
    "title":"30",
    "scenarioDescription":"30"
  },
  {
    "objectCode":"SCENARIO_31",
    "difficulty": 1,
    "title":"31",
    "scenarioDescription":"31"
  },
  {
    "objectCode":"SCENARIO_32",
    "difficulty": 1,
    "title":"32",
    "scenarioDescription":"32"
  },
  {
    "objectCode":"SCENARIO_33",
    "difficulty": 1,
    "title":"33",
    "scenarioDescription":"33"
  },
  {
    "objectCode":"SCENARIO_34",
    "difficulty": 1,
    "title":"324",
    "scenarioDescription":"34"
  },
  {
    "objectCode":"SCENARIO_35",
    "difficulty": 1,
    "title":"35",
    "scenarioDescription":"35"
  }
*/