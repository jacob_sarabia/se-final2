/**
 * Created by vincilbishop on 2/25/15.
 */

Meteor.startup(function() {

  // Scenario
  var scenarioText = Assets.getText ("db/scenarios.json");
  var scenarios = JSON.parse (scenarioText);
  scenarios.forEach (function (scenario) {
    var exists = typeof Scenario.findOne ({ objectCode: scenario.objectCode })  === 'object';

    if (!exists) {

      Scenario.insert(scenario);
    }
  });

  // NPCharacter
  var npCharacterText = Assets.getText ("db/npCharacters.json");
  var npCharacters = JSON.parse (npCharacterText);
  npCharacters.forEach (function (npCharacter) {
    var exists = typeof NPCharacter.findOne ({ objectCode: npCharacter.objectCode })  === 'object';

    if (!exists) {

      NPCharacter.insert(npCharacter);
    }
  });

  // NPCharacter Request
  var npCharacterRequestText = Assets.getText ("db/npCharacterRequests.json");
  var npCharacterRequests = JSON.parse (npCharacterRequestText);
  npCharacterRequests.forEach (function (npCharacterRequest) {
    var exists = typeof PlayerResponse.findOne ({ objectCode: npCharacterRequest.objectCode })  === 'object';

    if (!exists) {

      NPCharacterRequest.insert(npCharacterRequest);
    }
  });

  // NPCharacter Response

  // PlayerRequest

  // PlayerResponse
  var playerResponseText = Assets.getText ("db/playerResponses.json");
  var playerResponses = JSON.parse (playerResponseText);
  playerResponses.forEach (function (playerResponse) {
    var exists = typeof PlayerResponse.findOne ({ objectCode: playerResponse.objectCode })  === 'object';

    if (!exists) {

      PlayerResponse.insert(playerResponse);
    }
  });
  
  /*
  [
  {
    "objectCode":"WHY",
    "damageNum":1,
    "title":"No thanks, I just went shopping.",
    "npCharacterRequestCodes":["GREETING"]
  },
  {
    "objectCode":"NO",
    "damageNum":2,
    "title":"Not with those prices!",
    "npCharacterRequestCodes":["GREETING"]
  },
  {
    "objectCode":"YES",
    "damageNum":4,
    "title":"Absolutely! That +10 Fireblend looks like a steal!.",
    "npCharacterRequestCodes":["GREETING"]
  },
  {
    "objectCode":"YES2",
    "damageNum":5,
    "title":"Yup! Look at this!",
    "npCharacterRequestCodes":["FOUND_EVERYTHING_QUESTION"]
  },
  {
    "objectCode":"NO2",
    "damageNum":4,
    "title":"Not even close.",
    "npCharacterRequestCodes":["FOUND_EVERYTHING_QUESTION"]
  },
  {
    "objectCode":"THEY_R_MINE",
    "damageNum":1,
    "title":"What's it to ya?",
    "npCharacterRequestCodes":["FOUND_EVERYTHING_QUESTION"]
  },
  {
    "objectCode":"NO_COW",
    "damageNum":7,
    "title":"Sorry, not today.",
    "npCharacterRequestCodes":["PRODUCE_SPECIAL"]
  },
  {
    "objectCode":"TWO_BEES",
    "damageNum":11,
    "title":"Hmm, no change but here's a +4 Arc Wand you coud sell.!",
    "npCharacterRequestCodes":["PRODUCE_SPECIAL"]
  },
  {
    "objectCode":"CONFUSED",
    "damageNum":0,
    "title":"I have a personal Alcehmist. No thanks.",
    "npCharacterRequestCodes":["MEAT_SPECIAL"]
  },
  {
    "objectCode":"BECAUSE",
    "damageNum":4,
    "title":"I'm all out of Zennies.",
    "npCharacterRequestCodes":["MEAT_SPECIAL"]
  },
  {
    "objectCode":"COW_COW",
    "damageNum":12,
    "title":"Of course! I'm always glad to help out new Alchemists.",
    "npCharacterRequestCodes":["MEAT_SPECIAL"]
  },
  {
    "objectCode":"NOT_ME",
    "damageNum":-5,
    "title":"That's exactly what I didn't do.",
    "npCharacterRequestCodes":["CASH_CREDIT"]
  },
  {
    "objectCode":"THEM",
    "damageNum":6,
    "title":"Sorry, I'm not feeling very generous today.",
    "npCharacterRequestCodes":["CASH_CREDIT"]
  },
  {
    "objectCode":"MURDER",
    "damageNum":4,
    "title":"Cutting and running. CUTTING and RUNNING!",
    "npCharacterRequestCodes":["ACCUSATION"]
  },
  {
    "objectCode":"THEFT",
    "damageNum":11,
    "title":"It's do or die. I just hope my supply of Holy Water will be enough.",
    "npCharacterRequestCodes":["ACCUSATION"]
  },
  {
    "objectCode":"INNOCENT",
    "damageNum":5,
    "title":"If this is my fate, so be it. Jusr make it fast...",
    "npCharacterRequestCodes":["ACCUSATION"]
  }
]
  */

/*[
  {
    "objectCode":"GREETING",
    "title":"Care for some supplies traveller?",
    "difficulty": 2,
    "playerResponseCodes":["WHY","NO", "YES"]
  },
  {
    "objectCode":"FOUND_EVERYTHING_QUESTION",
    "title":"Did you purchase something from that merchant?",
    "difficulty": 4,
    "playerResponseCodes":["YES2","NO2", "THEY_R_MINE"]
  },
  {
    "objectCode":"PRODUCE_SPECIAL",
    "title":"Stranger, could you spare some change?",
    "difficulty": 9,
    "playerResponseCodes":["NO_COW","TWO_BEE"]
  },
  {
    "objectCode":"MEAT_SPECIAL",
    "title":"Would you care for some cheap potions? I can make you some.",
    "difficulty": 3,
    "playerResponseCodes":["CONFUSED", "BECAUSE", "COW_COW"]
  },
  {
    "objectCode":"CASH_CREDIT",
    "title":"I hope you have't spent a lot of money around town. The church could use your donation.",
    "difficulty": 7,
    "playerResponseCodes":["NOT_ME","THEM"]
  },
  {
    "objectCode":"ACCUSATION",
    "title":"How dare you, blasphemer?! I will make you pay your tithe in blood!",
    "difficulty": 7,
    "playerResponseCodes":["MURDER","THEFT","INNOCENT"]
  }
]
*/

/*[
  {
    "objectCode":"STORE_MANAGER",
    "title":"Merchant",
    "level": 2,
    "npCharacterRequestCodes":["GREETING","FOUND_EVERYTHING_QUESTION","PRODUCE_SPECIAL","MEAT_SPECIAL","CASH_CREDIT","ACCUSATION"],
    "scenarioCodes":["CHECKOUT_LANE"]

  }
  
]
*/

/*
,"SLEEP_WHEN","SOCIABLE","SLEEP_WHY","ANX_ACT1-1","ANX_ACT1-2","ANX_SLEEP","ANX_ACT2-1_Dream","ANX_ACT2-1_Transition","ANX_ACT2-2_Dream","ANX_ACT2-2_Transition","ANX_ACT3-1_DreamEnding","ANX_ACT3-2_RealEnding","ANX_ACT3-3_RealEnding","STS_ACT1-1","STS_ACT1-2","STS_SLEEP","STS_ACT2-1_Dream","STS_ACT2-1_Transition","STS_ACT2-2_Dream","STS_ACT2-2_Transition","STS_ACT3-1_DreamEnding","STS_ACT3-2_RealEnding","STS_ACT3-3_RealEnding","TRM_ACT1-1","TRM_ACT1-2","TRM_SLEEP","TRM_ACT2-1_Dream","TRM_ACT2-1_Transition","TRM_ACT2-2_Dream","TRM_ACT2-2_Transition","TRM_ACT3-1_DreamEnding","TRM_ACT3-2_RealEnding","TRM_ACT3-3_RealEnding","FILL"],
   
*/
});